const helpers = require('./helpers');
const path = require('path');

module.exports = function (options) {
  return {

    devtool: 'inline-source-map',

    resolve: {

      extensions: ['.ts', '.js'],

      modules: [path.resolve(__dirname, 'src'), 'node_modules']

    },

    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.js$/,
          loader: 'source-map-loader',
          exclude: [
            helpers.root('node_modules/rxjs'),
            helpers.root('node_modules/@angular')
          ]
        },

        {
          test: /\.ts$/,
          use: [
            {
              loader: 'awesome-typescript-loader',
              query: {
                sourceMap: false,
                inlineSourceMap: true,
                compilerOptions: {
                  removeComments: true
                }
              },
            },
            'angular2-template-loader'
          ],
          exclude: [/\.e2e\.ts$/]
        },

        {
          test: /\.json$/,
          loader: 'json-loader',
          exclude: [helpers.root('src/index.html')]
        },

        {
          test: /\.css$/,
          loader: ['to-string-loader', 'raw-loader', 'postcss-loader'],
          exclude: [helpers.root('src', 'styles')]
        },

        {
          test: /\.scss$/,
          use: ['to-string-loader', 'raw-loader', 'postcss-loader', 'sass-loader'],
          exclude: [helpers.root('src', 'scss')]
        },

        {
          test: /\.html$/,
          loader: 'raw-loader',
          exclude: [helpers.root('src/index.html')]
        },

        {
          enforce: 'post',
          test: /\.(js|ts)$/,
          loader: 'istanbul-instrumenter-loader',
          include: helpers.root('src'),
          exclude: [
            /\.(e2e|spec)\.ts$/,
            /node_modules/
          ]
        }

      ],
      loaders: [
        {
          test: /\.ts$/,
          loader: ['istanbul-instrumenter-loader'],
          exclude: [
            'node_modules',
            /\.spec\.ts$/
          ]
        },
        { test: /\.ts?$/, loader: 'ts-loader' }
      ]
    },

    performance: {
      hints: false
    },

    node: {
      global: true,
      process: false,
      crypto: 'empty',
      module: false,
      clearImmediate: false,
      setImmediate: false
    }

  };
}
