module.exports = function (config) {
  var testWebpackConfig = require('./webpack.test.js')({ env: 'test' });

  var configuration = {

    basePath: '',

    frameworks: ['jasmine'],

    exclude: [],

    client: {
      captureConsole: false
    },

    files: [
      { pattern: './test-config/spec-bundle.js', watched: false },
    ],

    preprocessors: { './test-config/spec-bundle.js': ['coverage', 'webpack', 'sourcemap'] },

    webpack: testWebpackConfig,

    coverageReporter: {
      type: 'in-memory'
    },

    remapCoverageReporter: {
      'text-summary': null,
      json: './coverage/coverage.json',
      lcovonly: './coverage/lcov.info',
      html: './coverage/html'
    },


    webpackMiddleware: {
      noInfo: true,
      stats: {
        chunks: false
      }
    },

    reporters: ['mocha', 'coverage', 'remap-coverage'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: [
      'Chrome'
    ],
    singleRun: true
  };

  config.set(configuration);
};
