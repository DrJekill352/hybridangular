const path = require('path');
module.exports = () => {
    return {
        entry: {
            'vendor': './src/vendor.ts',
            'polyfills': './src/polyfills.ts',
            'main': './src/main.ts'
        },

        output: {
            path: path.resolve('./build'),
            filename: '[name].bundle.js',
            chunkFilename: '[id].chunk.js',
            sourceMapFilename: '[name].map'
        },
        resolve: {
            extensions: ['.js', '.ts', '.html']
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    loaders: [
                        'awesome-typescript-loader',
                        'angular2-template-loader'
                    ]
                },
                {
                    test: /\.html$/,
                    loader: 'html-loader'
                }
            ]
        },
        devtool: 'inline-source-map'
    };
};