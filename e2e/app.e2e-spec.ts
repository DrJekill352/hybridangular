import { browser, by, element, promise, ElementFinder, ElementArrayFinder } from 'protractor';

describe('test-project Hybrid Angular App', () => {

  beforeEach(() => {
    browser.get('/');
  });

  it('should display Title application', async () => {
    let elementText: promise.Promise<string> = element(by.css('.todo-app__title')).getText();
    await expect(elementText).toEqual('My ToDos');
  });

  it('edit name to init todo list', async () => {
    let editTitleNameButtonElement: ElementFinder = element(by.css('.input-title__edit-button'));
    await editTitleNameButtonElement.click();
    let editTitleNameInputElement: ElementFinder = element(by.css('.input-title__input input'));
    await editTitleNameInputElement.sendKeys(' New');
    let inputElement: ElementFinder = element(by.css('.input-title__input input'));
    await expect(inputElement.getAttribute('value')).toEqual('My first ToDo list New');
    await editTitleNameButtonElement.click();
  });

  it('edit values in todo list', async () => {
    let editTodoButtonElements: ElementArrayFinder = element.all(by.css('.todo__edit-button'));
    await editTodoButtonElements.click();
    let editTodoInputElements: ElementArrayFinder = element.all(by.css('.todo__input > input'));
    await editTodoInputElements.first().sendKeys(' New');
    await editTodoButtonElements.first().click();
    await expect(editTodoInputElements.first().getAttribute('value')).toEqual('My first TODO New');
    await editTodoInputElements.last().sendKeys(' New');
    await editTodoButtonElements.last().click();
    await expect(editTodoInputElements.last().getAttribute('value')).toEqual('Them first TODO Item New');
  });

  it('edit check state todos in todo list', async () => {
    let editTodoEditElements: ElementArrayFinder = element.all(by.css('.todo__checkbox'));
    await editTodoEditElements.click();
    let firstCheckTodoElement: ElementFinder = editTodoEditElements.first();
    await expect(firstCheckTodoElement.getAttribute('class')).toContain('ng-empty');
    let secondCheckTodoElement: ElementFinder = editTodoEditElements.last();
    await expect(secondCheckTodoElement.getAttribute('class')).toContain('ng-not-empty');
  });

  it('delete all todo items in todo list', async () => {
    let deleteTodoButtonElements: ElementArrayFinder = element.all(by.css('.todo__delete-button'));
    let todoComponentsElements: ElementArrayFinder = element.all(by.css('ha-todo'));
    await expect(todoComponentsElements.count()).toEqual(2);
    await deleteTodoButtonElements.click();
    await expect(todoComponentsElements.count()).toEqual(0);
  });

  it('add new todo item in todo list', async () => {
    let addTodoButtonElement: ElementFinder = element(by.css('.todo-list__add-todo > ha-button > button'));
    let todoComponentsElements: ElementArrayFinder = element.all(by.css('ha-todo'));
    await expect(todoComponentsElements.count()).toEqual(2);
    await addTodoButtonElement.click();
    await expect(todoComponentsElements.count()).toEqual(3);
  });


  it('imitation real user work todo list', async () => {
    let deleteFirstTodoButtonElement: ElementFinder = element.all(by.css('.todo__delete-button')).first();
    let todoComponentsElements: ElementArrayFinder = element.all(by.css('ha-todo'));
    await expect(todoComponentsElements.count()).toEqual(2);
    await deleteFirstTodoButtonElement.click();
    await expect(todoComponentsElements.count()).toEqual(1);
    let addTodoListButtonElement: ElementFinder = element(by.css('.todo-list__add-todo > ha-button > button'));
    await expect(todoComponentsElements.count()).toEqual(1);
    await addTodoListButtonElement.click();
    await expect(todoComponentsElements.count()).toEqual(2);
    let editTodoInputLastElement: ElementFinder = element.all(by.css('.todo__input > input')).last();
    await editTodoInputLastElement.sendKeys('New Todo Item');
    let editTodoButtonLastElements: ElementFinder = element.all(by.css('.todo__edit-button')).last();
    await editTodoButtonLastElements.click();
    await expect(editTodoInputLastElement.getAttribute('value')).toEqual('New Todo Item');
    let editTodoEditFirstElements: ElementFinder = element.all(by.css('.todo__checkbox')).first();
    await editTodoEditFirstElements.click();
    await expect(editTodoEditFirstElements.getAttribute('class')).toContain('ng-not-empty');
    await expect(todoComponentsElements.count()).toEqual(2);
    await deleteFirstTodoButtonElement.click();
    await expect(todoComponentsElements.count()).toEqual(1);
    await editTodoEditFirstElements.click();
    await expect(editTodoEditFirstElements.getAttribute('class')).toContain('ng-not-empty');
    await expect(todoComponentsElements.count()).toEqual(1);
    await deleteFirstTodoButtonElement.click();
    await expect(todoComponentsElements.count()).toEqual(0);
    let editTitleNameButtonElement: ElementFinder = element(by.css('.input-title__edit-button'));
    await editTitleNameButtonElement.click();
    let editTitleNameInputElement: ElementFinder = element(by.css('.input-title__input input'));
    await editTitleNameInputElement.sendKeys(' New');
    let inputElement: ElementFinder = element(by.css('.input-title__input input'));
    await editTitleNameButtonElement.click();
    await expect(inputElement.getAttribute('value')).toEqual('My first ToDo list New');
    await expect(todoComponentsElements.count()).toEqual(0);
    await addTodoListButtonElement.click();
    await expect(todoComponentsElements.count()).toEqual(1);
  });
});
