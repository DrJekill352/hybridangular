import { IComponentOptions, Injectable, IControllerConstructor, IController } from 'angular';

export class ContainerComponent implements IComponentOptions {
    public templateUrl: string = 'src/containers/container/container.component.html';
    public controller: Injectable<IControllerConstructor> = ContainerComponentController;
}

class ContainerComponentController implements IController {

}
