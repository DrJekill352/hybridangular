import { module } from 'angular';

import { ContainerComponent } from './container.component';

export const ContainerModule: string = 'container-module';
module(ContainerModule, [])
    .component('haContainer', new ContainerComponent());
