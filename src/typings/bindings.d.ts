declare module '@bindings' {
    export interface Bindings {
        [prop: string]: string;
    }
}
