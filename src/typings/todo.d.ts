declare module '@todo-app/entities' {
    export interface Todo {
        id: string;
        description: string;
        isCompleted: boolean;
    }

    export interface Todos {
        name: string;
        todos: Todo[];
    }
}

declare module '@todo-app' {
    export * from '@todo-app/entities';
    export * from '@todo-app/store';
    export * from '@todo-app/components';
}