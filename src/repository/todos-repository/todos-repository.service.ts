import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { raw } from 'guid';

import { Todos, Todo } from '@todo-app';
import { todoList } from './todos-repository.mock';

@Injectable()
export class TodosRepositoryService {
    public getTodos(): Observable<Todos> {
        return Observable.of(todoList);
    }

    public updateTodosName(newName: string): Observable<Todos> {
        todoList.name = newName;
        return Observable.of(todoList);
    }

    public createNewTodo(): Observable<Todos> {
        let newTodo: Todo = {
            id: raw(),
            description: '',
            isCompleted: false
        };
        todoList.todos.push(newTodo);
        return Observable.of(todoList);
    }

    public deleteTodo(todo: Todo): Observable<Todos> {
        todoList.todos = todoList.todos.filter((value: Todo) => todo.id !== value.id);
        return Observable.of(todoList);
    }

    public updateTodo(todo: Todo): Observable<Todos> {
        let updateTodoIndex: number = todoList.todos.findIndex((value: Todo) => value.id === todo.id);
        todoList.todos[updateTodoIndex] = todo;
        return Observable.of(todoList);
    }

    public firstService(payload: number): Observable<number> {
        return Observable.of(payload);
    }

    public secondService(payload: number): Observable<boolean> {
        if (payload === 1) {
            return Observable.of(true);
        } else {
            return Observable.of(false);
        }
    }
}
