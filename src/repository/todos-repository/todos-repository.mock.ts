import { raw } from 'guid';

import { Todo, Todos } from '@todo-app';

let todos: Todo[] = [{
    id: raw(),
    description: 'My first TODO',
    isCompleted: true
}, {
    id: raw(),
    description: 'Them first TODO Item',
    isCompleted: false
}];

export let todoList: Todos = {
    name: 'My first ToDo list',
    todos: todos
};
