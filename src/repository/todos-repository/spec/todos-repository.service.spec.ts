import { TestBed, inject } from '@angular/core/testing';

import { Todos, Todo } from '@todo-app';
import { todoList } from '../todos-repository.mock';
import { TodosRepositoryService } from '../todos-repository.service';
import { Observable } from 'rxjs/Observable';

describe('TodosRepositoryService', () => {
    let service: TodosRepositoryService;

    beforeEach(() => TestBed.configureTestingModule({
        providers: [TodosRepositoryService]
    }));

    beforeEach(inject([TodosRepositoryService], (ser: TodosRepositoryService) => {
        service = ser;
    }));

    it('should work', () => {
        expect(service).toBeDefined();
    });

    it('should get todos', (done: DoneFn) => {
        let todosName: string = todoList.name;
        service.getTodos().subscribe((value: Todos) => {
            expect(value.name).toEqual(todosName);
            done();
        }).unsubscribe();
    });

    it('should create new todo', (done: DoneFn) => {
        let countTodo: number;
        service.getTodos().subscribe((value: Todos) => {
            countTodo = value.todos.length;
        }).unsubscribe();
        service.createNewTodo().subscribe((value: Todos) => {
            expect(value.todos.length - 1).toEqual(countTodo);
            done();
        }).unsubscribe();
    });

    it('should update todos name', (done: DoneFn) => {
        let todosName: string;
        service.getTodos().subscribe((value: Todos) => {
            todosName = value.name;
        }).unsubscribe();

        let newTodosName: string = 'My second ToDo list';
        service.updateTodosName(newTodosName).subscribe((value: Todos) => {
            expect(value.name).toEqual(newTodosName);
            done();
        }).unsubscribe();
    });

    it('should delete todo', (done: DoneFn) => {
        let indexFindTodo: number;

        let firstTodo: Todo = { id: '-2', description: 'hello', isCompleted: true };
        service.getTodos().subscribe((value: Todos) => {
            firstTodo = value.todos[0];
        }).unsubscribe();

        expect(firstTodo.id).not.toEqual('-2');

        service.deleteTodo(firstTodo).subscribe((value: Todos) => {
            indexFindTodo = value.todos.findIndex((todo: Todo) => todo.id === firstTodo.id);
            expect(indexFindTodo).toEqual(-1);
            done();
        }).unsubscribe();
    });

    it('should update todo', (done: DoneFn) => {
        let firstTodo: Todo = { id: '-2', description: 'hello', isCompleted: true };

        service.getTodos().subscribe((value: Todos) => {
            firstTodo = value.todos[0];
        });

        expect(firstTodo.id).not.toEqual('-2');
        firstTodo.isCompleted = true;

        service.updateTodo(firstTodo).subscribe((value: Todos) => {
            expect(value.todos[0].isCompleted).toEqual(true);
            done();
        });
    });

    it('should first service get payload', () => {
        let thread$: Observable<number> = Observable.of(1);
        expect(service.firstService(1)).toEqual(thread$);
    });

    it('should second service get true', () => {
        let thread$: Observable<boolean> = Observable.of(true);
        expect(service.secondService(1)).toEqual(thread$);
    });

    it('should second service get false', () => {
        let thread$: Observable<boolean> = Observable.of(false);
        expect(service.secondService(2)).toEqual(thread$);
    });
});
