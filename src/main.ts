import { module } from 'angular';

import { ContainerModule } from './containers';
import { ButtonModule, CheckboxModule, InputModule, TodoModule, TodosModule, TodoTitleModule } from './ng1-components';
import { DowngradeModule } from './downgrade.module';

export const NgModule: string = 'app';
module(NgModule, [
    ContainerModule,
    ButtonModule,
    CheckboxModule,
    InputModule,
    TodoModule,
    TodosModule,
    TodoTitleModule,
    DowngradeModule
]);

import './hybrid-module';


