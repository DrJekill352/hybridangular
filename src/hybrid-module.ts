import { NgModule, NgModuleRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { UpgradeModule } from '@angular/upgrade/static';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CommonModule } from '@angular/common';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { DropDownComponent, RadioButtonGroupComponent, } from './ng-components';
import { TodosRepositoryService } from './repository';
import { TodoEffects, TodosService } from './store';
import { reducers } from './store';

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forRoot(reducers, {}),
        EffectsModule.forRoot([
            TodoEffects
        ]),
        StoreDevtoolsModule.instrument({
            maxAge: 25
        })
    ],
    exports: [
        BrowserModule,
        UpgradeModule
    ],
    declarations: [
        DropDownComponent,
        RadioButtonGroupComponent
    ],
    providers: [
        TodosRepositoryService,
        TodosService
    ],
    entryComponents: [
        DropDownComponent,
        RadioButtonGroupComponent
    ]
})
export class AppModule {
    /* istanbul ignore next */
    public ngDoBootstrap(): void {
        // fix: need to be called
        // https://angular.io/guide/upgrade#bootstrapping-hybrid-applications
    }
}

window.onload = async (): Promise<void> => {
    let provider: NgModuleRef<AppModule> = await platformBrowserDynamic().bootstrapModule(AppModule);
    /* istanbul ignore next */
    provider.injector.get(UpgradeModule).bootstrap(document.body, ['app']);
};
