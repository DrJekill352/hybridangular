import { downgradeComponent, downgradeInjectable } from '@angular/upgrade/static';
import { module } from 'angular';

import { DropDownComponent, RadioButtonGroupComponent } from './ng-components';
import { TodosService } from './store';
import { TodosRepositoryService } from './repository';

export const DowngradeModule: string = 'downgrade-module';
module(DowngradeModule, [])
    .directive('dropdownComponent', downgradeComponent({ component: DropDownComponent }))
    .directive('radioButtonGroupComponent', downgradeComponent({ component: RadioButtonGroupComponent }))
    .factory('todosStoreService', downgradeInjectable(TodosService))
    .factory('todosServiceMock', downgradeInjectable(TodosRepositoryService));
