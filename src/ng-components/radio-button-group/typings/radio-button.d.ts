declare module '@todo-app/components/radio-button' {
    export interface RadioButton {
        description: string;
        id: number;
    }
}
