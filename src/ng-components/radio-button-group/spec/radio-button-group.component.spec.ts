import { TestBed, ComponentFixture } from '@angular/core/testing';

import { RadioButton } from '@todo-app';
import { RadioButtonGroupComponent } from '../radio-button-group.component';

describe('RadioButtonGroupComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [RadioButtonGroupComponent] });
    });

    it('should work', () => {
        let fixture: ComponentFixture<RadioButtonGroupComponent> = TestBed.createComponent(RadioButtonGroupComponent);
        expect(fixture.componentInstance instanceof RadioButtonGroupComponent).toBe(true, 'should create RadioButtonGroupComponent');
    });

    it('should on select', () => {
        let fixture: RadioButtonGroupComponent = new RadioButtonGroupComponent;
        let newRadioButton: RadioButton = {
            id: 1,
            description: 'new value'
        };
        expect(fixture.selectedRadioButton).not.toEqual(newRadioButton);
        fixture.onSelect(newRadioButton);
        expect(fixture.selectedRadioButton).toEqual(newRadioButton);
    });
});
