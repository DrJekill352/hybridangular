import { Component, Input, Output, EventEmitter } from '@angular/core';

import { RadioButton } from '@todo-app';

@Component({
    selector: 'ha-radio-button-group',
    templateUrl: './radio-button-group.component.html'
})
export class RadioButtonGroupComponent {
    @Input() public radioButtons: RadioButton[];
    @Output() public selectedRadioButtonChange: EventEmitter<RadioButton> = new EventEmitter();

    public selectedRadioButton: RadioButton | null = null;

    public onSelect(radioButton: RadioButton): void {
        this.selectedRadioButton = radioButton;
        this.selectedRadioButtonChange.emit(this.selectedRadioButton);
    }
}
