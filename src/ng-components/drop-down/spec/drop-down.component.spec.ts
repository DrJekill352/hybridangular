import { TestBed, ComponentFixture } from '@angular/core/testing';

import { DropDownComponent } from '../drop-down.component';

describe('DropDownComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [DropDownComponent] });
    });

    it('should work', () => {
        let fixture: ComponentFixture<DropDownComponent> = TestBed.createComponent(DropDownComponent);
        expect(fixture.componentInstance instanceof DropDownComponent).toBe(true, 'should create DropdownComponent');
    });

    it('should on select drop down option', () => {
        let fixture: DropDownComponent = new DropDownComponent;
        let newValue: string = 'new value';
        expect(fixture.selectedDropdownOption).not.toEqual(newValue);
        fixture.onSelect(newValue);
        expect(fixture.selectedDropdownOption).toEqual(newValue);
    });
});
