import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ha-drop-down',
    templateUrl: './drop-down.component.html'
})
export class DropDownComponent {
    @Input() public dropDownOptions: string[];
    @Output() public selectedDropDownOptionChange: EventEmitter<string> = new EventEmitter<string>();

    public selectedDropdownOption: string;

    public onSelect(newValue: string): void  {
        this.selectedDropdownOption = newValue;
        this.selectedDropDownOptionChange.emit(newValue);
    }
}
