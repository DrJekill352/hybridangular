import { raw } from 'guid';

import { Status, TodosState, Todo, Todos } from '@todo-app';
import {
    TodoActions,
    GET_TODOS,
    GET_TODOS_SUCCESS,
    GET_TODOS_FAILED,
    UPDATE_TODOS_NAME,
    UPDATE_TODOS_NAME_SUCCESS,
    UPDATE_TODOS_NAME_FAILED,
    CREATE_TODO, CREATE_TODO_SUCCESS,
    CREATE_TODO_FAILED, DELETE_TODO,
    DELETE_TODO_SUCCESS,
    DELETE_TODO_FAILED,
    UPDATE_TODO,
    UPDATE_TODO_SUCCESS,
    UPDATE_TODO_FAILED
} from './todo-action';

type State = TodosState;

const initialState: State = {
    status: Status.INITIAL,
    name: '',
    todos: []
};

export function todoReducer(state: State = initialState, action: TodoActions): State {
    switch (action.type) {
        case GET_TODOS: {
            return {
                ...state,
                status: Status.LOADING
            };
        }
        case GET_TODOS_SUCCESS: {
            let todos: Todos = action.payload as Todos;
            return {
                ...state,
                status: Status.SUCCESS,
                name: todos.name,
                todos: todos.todos
            };
        }
        case GET_TODOS_FAILED: {
            return {
                ...state,
                status: Status.ERROR
            };
        }
        case UPDATE_TODOS_NAME: {
            return {
                ...state,
                status: Status.UPDATING,
            };
        }
        case UPDATE_TODOS_NAME_SUCCESS: {
            let newName: string = action.payload as string;
            return {
                ...state,
                status: Status.SUCCESS,
                name: newName
            };
        }
        case UPDATE_TODOS_NAME_FAILED: {
            return {
                ...state,
                status: Status.ERROR,
            };
        }
        case CREATE_TODO: {
            return {
                ...state,
                status: Status.UPDATING
            };
        }
        case CREATE_TODO_SUCCESS: {
            let todos: Todos = action.payload as Todos;
            return {
                ...state,
                status: Status.SUCCESS,
                todos: todos.todos,
                name: todos.name
            };
        }
        case CREATE_TODO_FAILED: {
            return {
                ...state,
                status: Status.ERROR
            };
        }
        case DELETE_TODO: {
            return {
                ...state,
                status: Status.DELETING
            };
        }
        case DELETE_TODO_SUCCESS: {
            let deleteTodo: Todo = action.payload as Todo;
            return {
                ...state,
                status: Status.SUCCESS,
                todos: [
                    ...state.todos.filter((todo: Todo) => todo.id !== deleteTodo.id)
                ]
            };
        }
        case DELETE_TODO_FAILED: {
            return {
                ...state,
                status: Status.ERROR
            };
        }
        case UPDATE_TODO: {
            return {
                ...state,
                status: Status.UPDATING
            };
        }
        case UPDATE_TODO_SUCCESS: {
            let updateTodo: Todo = action.payload as Todo;
            let updateTodoIndex: number = state.todos.findIndex((value: Todo) => value.id === updateTodo.id);
            let todos: Todo[] = state.todos;
            todos[updateTodoIndex] = updateTodo;
            return {
                ...state,
                status: Status.SUCCESS,
                todos: todos
            };
        }
        case UPDATE_TODO_FAILED: {
            return {
                ...state,
                status: Status.ERROR
            };
        }
        default: {
            return state;
        }
    }
}
