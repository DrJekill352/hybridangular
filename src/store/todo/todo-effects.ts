import { Injectable } from '@angular/core';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs';

import { Todos, Todo } from '@todo-app';
import { TodosRepositoryService } from '../../repository';
import {
  GET_TODOS,
  GetTodosSuccess,
  GetTodosFailed,
  UPDATE_TODOS_NAME,
  UpdateTodosNameSuccess,
  UpdateTodosNameFailed,
  CREATE_TODO,
  CreateTodoSuccess,
  CreateTodoFailed,
  DELETE_TODO,
  DeleteTodoSuccess,
  DeleteTodoFailed,
  UPDATE_TODO,
  UpdateTodoSuccess,
  UpdateTodoFailed,
  GET_TODOS_FAILED,
  UPDATE_TODOS_NAME_FAILED,
  CREATE_TODO_FAILED,
  UPDATE_TODO_FAILED,
  DELETE_TODO_FAILED
} from './todo-action';
import { Action } from 'rxjs/scheduler/Action';

@Injectable()
export class TodoEffects {

  @Effect() public getTodos$: Observable<GetTodosSuccess | GetTodosFailed> = this.actions$
    .ofType(GET_TODOS)
    .switchMap(() => this.todosRepositoryService.getTodos()
      .map((todos: Todos) => new GetTodosSuccess(todos))
      .catch(() => Observable.of(new GetTodosFailed())
      ));

  @Effect() public updateTodosName$: Observable<UpdateTodosNameSuccess | UpdateTodosNameFailed> = this.actions$
    .ofType(UPDATE_TODOS_NAME)
    .map(toPayload)
    .switchMap((payload: string) => this.todosRepositoryService.updateTodosName(payload)
      .map((newPayload: Todos) => new UpdateTodosNameSuccess(newPayload.name))
      .catch(() => Observable.of(new UpdateTodosNameFailed())
      ));

  @Effect() public createTodo$: Observable<CreateTodoSuccess | CreateTodoFailed> = this.actions$
    .ofType(CREATE_TODO)
    .switchMap(() => this.todosRepositoryService.createNewTodo()
      .map((todos: Todos) => new CreateTodoSuccess(todos))
      .catch(() => Observable.of(new CreateTodoFailed())
      ));

  @Effect() public deleteTodo$: Observable<DeleteTodoSuccess | DeleteTodoFailed> = this.actions$
    .ofType(DELETE_TODO)
    .map(toPayload)
    .switchMap((payload: Todo) => this.todosRepositoryService.deleteTodo(payload)
      .map(() => new DeleteTodoSuccess(payload))
      .catch(() => Observable.of(new DeleteTodoFailed())
      ));

  @Effect() public updateTodo$: Observable<UpdateTodoSuccess | UpdateTodoFailed> = this.actions$
    .ofType(UPDATE_TODO)
    .map(toPayload)
    .switchMap((payload: Todo) => this.todosRepositoryService.updateTodo(payload)
      .map(() => new UpdateTodoSuccess(payload))
      .catch(() => Observable.of(new UpdateTodoFailed())
      ));

  public constructor(
    private actions$: Actions,
    private todosRepositoryService: TodosRepositoryService
  ) { }

  // tslint:disable-next-line:member-ordering
  @Effect() public godEffect$: Observable<{ type: string }> = this.actions$
    .ofType(GET_ACTION)
    .map(toPayload)
    .switchMap((payload: number) => this.todosRepositoryService.firstService(payload)
      .switchMap((value: number) => {
        if (value === 0) {
          return Observable.of({ type: FIRST_ACTION });
        } else {
          return this.todosRepositoryService.secondService(value)
            .switchMap((boolValue: boolean) => {
              if (boolValue) {
                return Observable.of({ type: SECOND_ACTION });
              } else {
                return Observable.of({ type: THIRD_ACTION });
              }
            })
            .catch(() => Observable.of({ type: GET_SECOND_ACTION_FAIL }));
        }
      })
      .catch((error: Error) => Observable.of({ type: GET_FIRST_ACTION_FAIL })));
}

export const GET_ACTION: string = 'GET_ACTION';

export const FIRST_ACTION: string = 'FIRST_ACTION';
export const SECOND_ACTION: string = 'SECOND_ACTION';
export const THIRD_ACTION: string = 'THIRD_ACTION';

export const GET_FIRST_ACTION_FAIL: string = 'GET_FIRST_ACTION_FAIL';
export const GET_SECOND_ACTION_FAIL: string = 'GET_SECOND_ACTION_FAIL';
