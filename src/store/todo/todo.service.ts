import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Todo, Todos, State } from '@todo-app';
import { getTodos } from './todo-selector';
import {
    GetTodos,
    CreateTodo,
    DeleteTodo,
    UpdateTodo,
    UpdateTodosName
} from './todo-action';


@Injectable()
export class TodosService {

    public constructor(private store: Store<State>) { }

    public getTodos(): Observable<Todos> {
        this.store.dispatch(new GetTodos());
        return this.store.select(getTodos);
    }

    public createNewTodo(): void {
        this.store.dispatch(new CreateTodo());
    }

    public deleteTodo(todo: Todo): void {
        this.store.dispatch(new DeleteTodo(todo));
    }

    public updateTodo(todo: Todo): void {
        this.store.dispatch(new UpdateTodo(todo));
    }

    public updateTodosName(newTodosName: string): void {
        this.store.dispatch(new UpdateTodosName(newTodosName));
    }
}
