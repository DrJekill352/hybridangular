import { TestBed, inject } from '@angular/core/testing';
import { StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { Todos, Todo } from '@todo-app';
import { TodoEffects } from '../todo-effects';
import { TodosService } from '../todo.service';
import { TodosRepositoryService } from '../../../repository';
import { reducers } from '../../store.config';

describe('TodosService', () => {
    let service: TodosService;

    beforeEach(() => TestBed.configureTestingModule({
        imports: [StoreModule.forRoot(reducers, {}), EffectsModule.forRoot([TodoEffects])],
        providers: [TodosRepositoryService, Store, TodosService]
    }).compileComponents());
    beforeEach(inject([TodosService], (ser: TodosService) => {
        service = ser;
    }));

    it('should work', () => {
        expect(service).toBeDefined();
    });

    it('should get todos', (done: DoneFn) => {
        service.getTodos().subscribe((todos: Todos) => {
            expect(todos.name).not.toBeUndefined();
            done();
        }).unsubscribe();
    });

    it('should create new todo', (done: DoneFn) => {
        let countTodo: number = 0;
        service.getTodos().subscribe((todos: Todos) => {
            countTodo = todos.todos.length;
            expect(countTodo).not.toEqual(0);
        }).unsubscribe();

        service.createNewTodo();
        countTodo++;

        service.getTodos().subscribe((todos: Todos) => {
            expect(todos.todos.length).toEqual(countTodo);
            done();
        }).unsubscribe();
    });

    it('should delete todo', (done: DoneFn) => {
        let deletingTodo: Todo = {
            id: 'NaN',
            description: 'NaN',
            isCompleted: false
        };
        service.getTodos().subscribe((todos: Todos) => {
            deletingTodo = todos.todos[0];
            expect(deletingTodo.id).not.toEqual('NaN');
        }).unsubscribe();
        service.deleteTodo(deletingTodo);
        service.getTodos().subscribe((todos: Todos) => {
            expect(todos).not.toContain(deletingTodo);
            done();
        }).unsubscribe();
    });

    it('should update todo list name', (done: DoneFn) => {
        let newTodosName: string = 'My third ToDo list';
        service.getTodos().subscribe((todos: Todos) => {
            expect(todos.name).not.toEqual(newTodosName);
        }).unsubscribe();

        service.updateTodosName(newTodosName);

        service.getTodos().subscribe((todos: Todos) => {
            expect(todos.name).toEqual(newTodosName);
            done();
        }).unsubscribe();
    });

    it('should update todo', (done: DoneFn) => {
        let updateTodo: Todo = {
            id: 'NaN',
            description: 'NaN',
            isCompleted: false
        };
        service.getTodos().subscribe((todos: Todos) => {
            updateTodo = Object.assign({}, todos.todos[0]);
            expect(updateTodo.id).not.toEqual('NaN');
        }).unsubscribe();
        let value: boolean = updateTodo.isCompleted;
        updateTodo.isCompleted = !updateTodo.isCompleted;
        service.updateTodo(updateTodo);
        service.getTodos().subscribe((todos: Todos) => {
            expect(todos.todos[0].isCompleted).not.toEqual(value);
            done();
        }).unsubscribe();
    });
});
