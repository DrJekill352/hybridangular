import { TodoEffects, GET_ACTION, FIRST_ACTION } from '../todo-effects';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { EffectsModule } from '@ngrx/effects';
import { TodosRepositoryService } from '../../../repository';
import { StoreModule } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { reducers } from '../../store.config';
import { Action } from 'rxjs/scheduler/Action';
import { TestColdObservable } from 'jasmine-marbles/src/test-observables';
import { cold } from 'jasmine-marbles';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { SECOND_ACTION, GET_FIRST_ACTION_FAIL, THIRD_ACTION, GET_SECOND_ACTION_FAIL } from '../../index';
import { TodosService } from '../todo.service';


describe('TodoGodEffect', () => {

    it('should get first action', () => {
        const source: TestColdObservable = cold('a', { a: { type: GET_ACTION, payload: 0 } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['firstService']);

        const serviceResponse: Observable<number> = Observable.of(0);

        service.firstService.and.returnValue(serviceResponse);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);

        const expected: TestColdObservable = cold('a', { a: { type: FIRST_ACTION } });
        expect(effects.godEffect$).toBeObservable(expected);
    });

    it('should get first action fail', () => {
        const source: TestColdObservable = cold('a', { a: { type: GET_ACTION, payload: 0 } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['firstService']);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);

        const serviceResponse: ErrorObservable = Observable.throw(new Error('Error first service fail!'));

        service.firstService.and.returnValue(serviceResponse);

        const expected: TestColdObservable = cold('a', { a: { type: GET_FIRST_ACTION_FAIL } });
        expect(effects.godEffect$).toBeObservable(expected);
    });

    it('should get second action', () => {
        const source: TestColdObservable = cold('a', { a: { type: GET_ACTION, payload: 1 } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['firstService', 'secondService']);

        const serviceFirstResponse: Observable<number> = Observable.of(1);
        const serviceSecondResponse: Observable<boolean> = Observable.of(true);

        service.firstService.and.returnValue(serviceFirstResponse);
        service.secondService.and.returnValue(serviceSecondResponse);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);
        const expected: TestColdObservable = cold('a', { a: { type: SECOND_ACTION } });
        expect(effects.godEffect$).toBeObservable(expected);
    });

    it('should get third action', () => {
        const source: TestColdObservable = cold('a', { a: { type: GET_ACTION, payload: 2 } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['firstService', 'secondService']);

        const serviceFirstResponse: Observable<number> = Observable.of(2);
        const serviceSecondResponse: Observable<boolean> = Observable.of(false);

        service.firstService.and.returnValue(serviceFirstResponse);
        service.secondService.and.returnValue(serviceSecondResponse);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);
        const expected: TestColdObservable = cold('a', { a: { type: THIRD_ACTION } });
        expect(effects.godEffect$).toBeObservable(expected);
    });

    it('should get second action fail', () => {
        const source: TestColdObservable = cold('a', { a: { type: GET_ACTION, payload: 1 } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['firstService', 'secondService']);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);

        const serviceFirstResponse: Observable<number> = Observable.of(2);
        const serviceSecondResponse: ErrorObservable = Observable.throw(new Error('Error first service fail!'));

        service.firstService.and.returnValue(serviceFirstResponse);
        service.secondService.and.returnValue(serviceSecondResponse);

        const expected: TestColdObservable = cold('a', { a: { type: GET_SECOND_ACTION_FAIL } });
        expect(effects.godEffect$).toBeObservable(expected);
    });
});
