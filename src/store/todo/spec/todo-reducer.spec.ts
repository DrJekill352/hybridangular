import { TestBed } from '@angular/core/testing';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store, State } from '@ngrx/store';
import { raw } from 'guid';

import { TodosState, Status, Todo } from '@todo-app';
import { reducers } from '../../store.config';
import { TodoEffects } from '../todo-effects';
import { TodosRepositoryService } from '../../../repository';
import { todoReducer } from '../todo-reducer';
import { GetTodosFailed, UpdateTodosNameFailed, DeleteTodoFailed, CreateTodoFailed, UpdateTodoFailed } from '../todo-action';

describe('TodosService', () => {
    const initialState: TodosState = {
        status: Status.INITIAL,
        name: '',
        todos: []
    };

    it('should get todos fail', () => {
        let reducer: TodosState = todoReducer(initialState, new GetTodosFailed(Error('get todos fail')));
        expect(reducer.status).toEqual(Status.ERROR);
    });

    it('should update todo list fail', () => {
        let reducer: TodosState = todoReducer(initialState, new UpdateTodosNameFailed(Error('update todo fail')));
        expect(reducer.status).toEqual(Status.ERROR);
    });

    it('should create todo fail', () => {
        let reducer: TodosState = todoReducer(initialState, new CreateTodoFailed(Error('create todo fail')));
        expect(reducer.status).toEqual(Status.ERROR);
    });

    it('should delete todo fail', () => {
        let reducer: TodosState = todoReducer(initialState, new DeleteTodoFailed(Error('delete todo fail')));
        expect(reducer.status).toEqual(Status.ERROR);
    });

    it('should update todo fail', () => {
        let reducer: TodosState = todoReducer(initialState, new UpdateTodoFailed(Error('update todo fail')));
        expect(reducer.status).toEqual(Status.ERROR);
    });
});
