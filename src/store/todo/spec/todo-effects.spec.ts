import { TestBed, async } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { cold } from 'jasmine-marbles';

import { TestColdObservable } from 'jasmine-marbles/src/test-observables';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { TodosService } from '../todo.service';
import { TodoEffects } from '../todo-effects';
import { TodosRepositoryService } from '../../../repository';
import {
    GET_TODOS,
    GET_TODOS_FAILED,
    UPDATE_TODOS_NAME,
    UPDATE_TODOS_NAME_FAILED,
    UpdateTodo, UPDATE_TODO,
    UPDATE_TODO_FAILED,
    UPDATE_TODOS_NAME_SUCCESS,
    DELETE_TODO,
    DELETE_TODO_FAILED,
    CREATE_TODO,
    CREATE_TODO_FAILED
} from '../../todo';
import { GetTodosFailed, UpdateTodoFailed, DeleteTodoFailed, CreateTodoFailed, UpdateTodosNameFailed, GetTodos } from '../../index';


describe('TodoEffects', () => {

    it('should get todos fail', () => {
        const source: TestColdObservable = cold('a', { a: new GetTodos() });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['getTodos']);

        const serviceResponse: ErrorObservable = Observable.throw(new Error('Error get todos!'));

        service.getTodos.and.returnValue(serviceResponse);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);

        const expected: TestColdObservable = cold('a', { a: new GetTodosFailed() });
        expect(effects.getTodos$).toBeObservable(expected);
    });

    it('should update todo fail', () => {
        const source: TestColdObservable = cold('a', { a: { type: UPDATE_TODO } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['updateTodo']);

        const serviceResponse: ErrorObservable = Observable.throw(new Error('Error update todo!'));

        service.updateTodo.and.returnValue(serviceResponse);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);

        const expected: TestColdObservable = cold('a', { a: new UpdateTodoFailed() });
        expect(effects.updateTodo$).toBeObservable(expected);
    });

    it('should delete todo fail', () => {
        const source: TestColdObservable = cold('a', { a: { type: DELETE_TODO } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['deleteTodo']);

        const serviceResponse: ErrorObservable = Observable.throw(new Error('Error delete todo!'));

        service.deleteTodo.and.returnValue(serviceResponse);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);

        const expected: TestColdObservable = cold('a', { a: new DeleteTodoFailed() });
        expect(effects.deleteTodo$).toBeObservable(expected);
    });

    it('should create todo fail', () => {
        const source: TestColdObservable = cold('a', { a: { type: CREATE_TODO } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['createNewTodo']);

        const serviceResponse: ErrorObservable = Observable.throw(new Error('Error create todo!'));

        service.createNewTodo.and.returnValue(serviceResponse);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);

        const expected: TestColdObservable = cold('a', { a: new CreateTodoFailed() });
        expect(effects.createTodo$).toBeObservable(expected);
    });

    it('should update todos name fail', () => {
        const source: TestColdObservable = cold('a', { a: { type: UPDATE_TODOS_NAME } });

        let service: jasmine.SpyObj<TodosRepositoryService> = jasmine
            .createSpyObj<TodosRepositoryService>('todosRepositoryService', ['updateTodosName']);

        const serviceResponse: ErrorObservable = Observable.throw(new Error('Error update todos name!'));

        service.updateTodosName.and.returnValue(serviceResponse);

        const effects: TodoEffects = new TodoEffects(new Actions(source), service);

        const expected: TestColdObservable = cold('a', { a: new UpdateTodosNameFailed() });
        expect(effects.updateTodosName$).toBeObservable(expected);
    });
});
