declare module '@todo-app/store/todo' {
    import { Status } from '@todo-app/store';
    import { Todo } from '@todo-app/entities';

    export interface TodosState {
        status: Status;
        name: string;
        todos: Todo[];
    }
}
