import { createSelector, MemoizedSelector } from '@ngrx/store';

import { TodosState, State, Todos } from '@todo-app';

const getAppTodo: (state: State) => TodosState = (state: State): TodosState => state.todo;

export const getTodos: MemoizedSelector<State, Todos> = createSelector(
    getAppTodo,
    (state: TodosState) => {
        return { name: state.name, todos: state.todos };
    }
);
