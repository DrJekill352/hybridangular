import { Action } from '@ngrx/store';

import { Todo, Todos } from '@todo-app';

export const GET_TODOS: string = '[TODOS] GET_TODOS';
export const GET_TODOS_SUCCESS: string = '[TODOS] GET_TODOS_SUCCESS';
export const GET_TODOS_FAILED: string = '[TODOS] GET_TODOS_FAILED';

export const UPDATE_TODOS_NAME: string = '[TODOS] UPDATE_TODOS_NAME';
export const UPDATE_TODOS_NAME_SUCCESS: string = '[TODOS] UPDATE_TODOS_NAME_SUCCESS';
export const UPDATE_TODOS_NAME_FAILED: string = '[TODOS] UPDATE_TODOS_NAME_FAILED';

export const CREATE_TODO: string = '[TODO] CREATE_TODO';
export const CREATE_TODO_SUCCESS: string = '[TODO] CREATE_TODO_SUCCESS';
export const CREATE_TODO_FAILED: string = '[TODO] CREATE_TODO_FAILED';

export const DELETE_TODO: string = '[TODO] DELETE_TODO';
export const DELETE_TODO_SUCCESS: string = '[TODO] DELETE_TODO_SUCCESS';
export const DELETE_TODO_FAILED: string = '[TODO] DELETE_TODO_FAILED';

export const UPDATE_TODO: string = '[TODO] UPDATE_TODO';
export const UPDATE_TODO_SUCCESS: string = '[TODO] UPDATE_TODO_SUCCESS';
export const UPDATE_TODO_FAILED: string = '[TODO] UPDATE_TODO_FAILED';

export class GetTodos implements Action {
    public readonly type: string = GET_TODOS;

    public payload: void;
}

export class GetTodosSuccess implements Action {
    public readonly type: string = GET_TODOS_SUCCESS;

    public constructor(public payload: Todos) { }
}

export class GetTodosFailed implements Action {
    public readonly type: string = GET_TODOS_FAILED;

    public constructor(public payload?: Error) { }
}

export class UpdateTodosName implements Action {
    public readonly type: string = UPDATE_TODOS_NAME;

    public constructor(public payload: string) { }
}

export class UpdateTodosNameSuccess implements Action {
    public readonly type: string = UPDATE_TODOS_NAME_SUCCESS;

    public constructor(public payload: string) { }
}

export class UpdateTodosNameFailed implements Action {
    public readonly type: string = UPDATE_TODOS_NAME_FAILED;

    public constructor(public payload?: Error) { }
}

export class CreateTodo implements Action {
    public readonly type: string = CREATE_TODO;

    public payload?: void;
}

export class CreateTodoSuccess implements Action {
    public readonly type: string = CREATE_TODO_SUCCESS;

    public constructor(public payload: Todos) { }
}

export class CreateTodoFailed implements Action {
    public readonly type: string = CREATE_TODO_FAILED;

    public constructor(public payload?: Error) { }
}

export class DeleteTodo implements Action {
    public readonly type: string = DELETE_TODO;

    public constructor(public payload: Todo) { }
}

export class DeleteTodoSuccess implements Action {
    public readonly type: string = DELETE_TODO_SUCCESS;

    public constructor(public payload: Todo) { }
}

export class DeleteTodoFailed implements Action {
    public readonly type: string = DELETE_TODO_FAILED;

    public constructor(public payload?: Error) { }
}

export class UpdateTodo implements Action {
    public readonly type: string = UPDATE_TODO;

    public constructor(public payload: Todo) { }
}

export class UpdateTodoSuccess implements Action {
    public readonly type: string = UPDATE_TODO_SUCCESS;

    public constructor(public payload: Todo) { }
}

export class UpdateTodoFailed implements Action {
    public readonly type: string = UPDATE_TODO_FAILED;

    public constructor(public payload?: Error) { }
}

export type TodoActions =
    | GetTodos
    | GetTodosSuccess
    | GetTodosFailed
    | UpdateTodosName
    | UpdateTodosNameSuccess
    | UpdateTodosNameFailed
    | CreateTodo
    | CreateTodoSuccess
    | CreateTodoFailed
    | DeleteTodo
    | DeleteTodoSuccess
    | DeleteTodoFailed
    | UpdateTodo
    | UpdateTodoSuccess
    | UpdateTodoFailed;
