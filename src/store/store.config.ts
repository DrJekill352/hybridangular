import { TodosState } from '@todo-app';
import { todoReducer } from './todo';

export const reducers: Object = {
    todo: todoReducer
};
