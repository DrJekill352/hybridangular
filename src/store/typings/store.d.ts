declare module '@todo-app/store' {
    import { TodosState } from '@todo-app/store/todo';
    export * from '@todo-app/store/todo';

    export interface State {
        todo: TodosState;
    }

    export const enum Status {
        INITIAL,
        LOADING,
        SUCCESS,
        ERROR,
        UPDATING,
        DELETING
    }
}
