import { IComponentOptions, IController, IControllerConstructor, Injectable } from 'angular';

import { Bindings } from '@bindings';
import { Todo, BUTTON_TEXT } from '@todo-app';

export class TodoComponent implements IComponentOptions {
    public templateUrl: string = `src/ng1-components/todo/todo.component.html`;
    public controller: Injectable<IControllerConstructor> = TodoComponentController;
    public bindings: Bindings = { todo: '<', onUpdate: '&', onDelete: '&' };
}
class TodoComponentController implements IController {
    public placeholderText: string = '';
    public textButton: string = 'Enter';
    public isDisable: boolean = false;

    private todo: Todo;
    private onUpdate: (obj: { todo: Todo }) => void;
    private onDelete: (obj: { todo: Todo }) => void;

    public onButtonClick(): void {
        if (this.textButton === BUTTON_TEXT.ENTER) {
            this.textButton = BUTTON_TEXT.EDIT;
            this.onEnterButtonClick();
        } else {
            this.textButton = BUTTON_TEXT.ENTER;
        }
        this.isDisable = !this.isDisable;
    }

    public checkDisable(): void {
        let inputText: string = this.todo.description;
        if (inputText === '') {
            this.textButton = BUTTON_TEXT.ENTER;
            this.isDisable = false;
        } else {
            this.textButton = BUTTON_TEXT.EDIT;
            this.isDisable = true;
        }
    }

    public onEnterButtonClick(): void {
        this.onUpdate({ todo: this.todo });
    }

    public onDeleteTodoButtonClick(): void {
        this.onDelete({ todo: this.todo });
    }

    public onCheckboxClick(): void {
        this.onUpdate({ todo: this.todo });
    }

    public editDescription(newValue: string): void {
        this.todo.description = newValue;
    }
}
