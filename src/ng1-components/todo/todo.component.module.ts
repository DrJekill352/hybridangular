import { module } from 'angular';

import { TodoComponent } from './todo.component';

export const TodoModule: string = 'todo-module';
module(TodoModule, [])
    .component('haTodo', new TodoComponent());
