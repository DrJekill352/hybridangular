import * as angular from 'angular';
import 'angular-mocks';
import { raw } from 'guid';

import { Todo } from '@todo-app';
import { TodoModule } from '../todo.component.module';

describe('TodoComponent', () => {
    let component: angular.IComponentController;
    let updateSpy: jasmine.Spy = jasmine.createSpy('updateSpy');
    let deleteSpy: jasmine.Spy = jasmine.createSpy('deleteSpy');

    beforeEach(angular.mock.module(TodoModule));

    beforeEach(inject(($componentController: angular.IComponentControllerService) => {
        let todo: Todo = {
            id: raw(),
            description: 'Ok',
            isCompleted: false
        };

        component = $componentController('haTodo', {}, { todo: todo, onUpdate: updateSpy, onDelete: deleteSpy });
    }));

    it('should work', () => {
        expect(component).toBeDefined();
    });

    it('should check disable with empty value', () => {
        component.todo.description = '';
        component.checkDisable();
        expect(component.textButton).toEqual('Enter');
    });

    it('should check disable with not empty value', () => {
        component.checkDisable();
        expect(component.textButton).toEqual('Edit');
    });

    it('should on button click with empty value', () => {
        component.textButton = 'Enter';
        component.onButtonClick();
        expect(component.textButton).toEqual('Edit');
    });

    it('should button click with not empty value', () => {
        component.textButton = 'Edit';
        component.onButtonClick();
        expect(component.textButton).toEqual('Enter');
    });

    it('should on delete todo button click', () => {
        let newTodo: Todo = {
            id: raw(),
            description: 'bay',
            isCompleted: true
        };

        component.todo = newTodo;
        component.onDeleteTodoButtonClick();
        expect(deleteSpy).toHaveBeenCalledWith({ todo: newTodo });
    });

    it('should on enter button click', () => {
        let newTodo: Todo = {
            id: raw(),
            description: 'bay',
            isCompleted: true
        };

        component.todo = newTodo;
        component.onEnterButtonClick();
        expect(updateSpy).toHaveBeenCalledWith({ todo: newTodo });
    });

    it('should on checkbox click', () => {
        let newTodo: Todo = {
            id: raw(),
            description: 'bay',
            isCompleted: true
        };

        component.todo = newTodo;
        component.onCheckboxClick();
        expect(updateSpy).toHaveBeenCalledWith({ todo: newTodo });
    });

    it('should edit description', () => {
        let newDescription: string = 'bay';
        component.editDescription(newDescription);
        expect(component.todo.description).toEqual(newDescription);
    });
});
