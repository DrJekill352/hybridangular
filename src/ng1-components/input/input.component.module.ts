import { module } from 'angular';

import { InputComponent } from './input.component';

export const InputModule: string = 'input-module';
module(InputModule, [])
    .component('haInput', new InputComponent());
