import * as angular from 'angular';
import 'angular-mocks';

import { InputModule } from '../input.component.module';

describe('InputComponent;', () => {
    let component: angular.IComponentController;
    let changeSpy: jasmine.Spy = jasmine.createSpy('changeSpy');

    beforeEach(angular.mock.module(InputModule));

    beforeEach(inject(($componentController: angular.IComponentControllerService) => {
        component = $componentController('haInput', {}, { value: 'hello', onEdit: changeSpy });
    }));

    it('should work', () => {
        expect(component).toBeDefined();
    });

    it('should on change', () => {
        component.onChange();
        expect(changeSpy).toHaveBeenCalledWith({ newValue: component.value });
    });
});
