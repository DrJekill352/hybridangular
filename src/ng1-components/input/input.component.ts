import { IComponentOptions, IController, IControllerConstructor, Injectable } from 'angular';

import { Bindings } from '@bindings';

export class InputComponent implements IComponentOptions {
    public templateUrl: string = `src/ng1-components/input/input.component.html`;
    public controller: Injectable<IControllerConstructor> = InputComponentController;
    public bindings: Bindings = { placeholder: '@', value: '<', isDisable: '<', onEdit: '&' };
}
class InputComponentController implements IController {
    public value: string;
    public onEdit: (obj: { newValue: string }) => void;

    public onChange(): void {
        this.onEdit({ newValue: this.value });
    }
}
