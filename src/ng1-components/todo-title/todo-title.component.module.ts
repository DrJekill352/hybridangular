import { module } from 'angular';

import { TodoTitleComponent } from './todo-title.component';

export const TodoTitleModule: string = 'todo-title-module';
module(TodoTitleModule, [])
    .component('haTodoTitle', new TodoTitleComponent());
