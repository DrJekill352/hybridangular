declare module '@todo-app/components/edit-button' {
    export const enum BUTTON_TEXT {
        EDIT = 'Edit',
        ENTER = 'Enter'
    }
}
