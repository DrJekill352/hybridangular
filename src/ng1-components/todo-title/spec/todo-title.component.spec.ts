import * as angular from 'angular';
import 'angular-mocks';

import { TodoTitleModule } from '../todo-title.component.module';

describe('TodoTitleComponent', () => {
    let component: angular.IComponentController;
    let updateSpy: jasmine.Spy = jasmine.createSpy('updateSpy');
    beforeEach(angular.mock.module(TodoTitleModule));

    beforeEach(inject(($componentController: angular.IComponentControllerService) => {
        component = $componentController('haTodoTitle', {}, { onUpdate: updateSpy });
    }));

    it('should work', () => {
        expect(component).toBeDefined();
    });

    it('should on update', () => {
        let newValue: string = 'hello world';
        component.value = newValue;
        component.onEnterButtonClick();
        expect(updateSpy).toHaveBeenCalledWith({ newTodosName: newValue });
    });

    it('should on edit', () => {
        let oldValue: string = component.value;
        let newValue: string = 'hello';
        expect(newValue).not.toEqual(oldValue);
        component.onEdit(newValue);
        expect(component.value).toEqual(newValue);
    });

    it('should check disable with empty value', () => {
        component.value = '';
        component.checkDisable();
        expect(component.textButton).toEqual('Enter');
    });

    it('should check disable with not empty value', () => {
        component.value = 'hello';
        component.checkDisable();
        expect(component.textButton).toEqual('Edit');
    });

    it('should on button click with empty value', () => {
        component.textButton = 'Enter';
        component.onButtonClick();
        expect(component.textButton).toEqual('Edit');
    });

    it('should button click with not empty value', () => {
        component.textButton = 'Edit';
        component.onButtonClick();
        expect(component.textButton).toEqual('Enter');
    });
});
