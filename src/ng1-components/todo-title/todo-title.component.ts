import { IController, IComponentOptions, IControllerConstructor, Injectable } from 'angular';

import { Bindings } from '@bindings';
import { BUTTON_TEXT } from '@todo-app';

export class TodoTitleComponent implements IComponentOptions {
    public templateUrl: string = `src/ng1-components/todo-title/todo-title.component.html`;
    public controller: Injectable<IControllerConstructor> = TodoTitleComponentController;
    public bindings: Bindings = { placeholder: '@', value: '<', onUpdate: '&' };
}
class TodoTitleComponentController implements IController {
    public textButton: string = 'Enter';
    public isDisable: boolean = false;

    private value: string;
    private onUpdate: (obj: { newTodosName: string }) => void;

    public onButtonClick(): void {
        if (this.textButton === BUTTON_TEXT.ENTER) {
            this.textButton = BUTTON_TEXT.EDIT;
            this.onEnterButtonClick();
        } else {
            this.textButton = BUTTON_TEXT.ENTER;
        }
        this.isDisable = !this.isDisable;
    }

    public checkDisable(): void {
        let inputText: string = this.value;
        if (inputText === '') {
            this.textButton = BUTTON_TEXT.ENTER;
            this.isDisable = false;
        } else {
            this.textButton = BUTTON_TEXT.EDIT;
            this.isDisable = true;
        }
    }

    public onEnterButtonClick(): void {
        this.onUpdate({ newTodosName: this.value });
    }

    public onEdit(newValue: string): void {
        this.value = newValue;
    }
}
