import { IController, IComponentOptions, IControllerConstructor, Injectable } from 'angular';

import { Bindings } from '@bindings';
import { Todo, Todos } from '@todo-app';
import { TodosService } from '../../store';
import { Observable } from 'rxjs';

export class TodosComponent implements IComponentOptions {
    public templateUrl: string = `src/ng1-components/todos/todos.component.html`;
    public controller: Injectable<IControllerConstructor> = TodosComponentController;
    public bindings: Bindings = {};
}
class TodosComponentController implements IController {

    public todos: Todos;
    public todos$: Observable<Todos>;

    private static $inject: string[] = ['todosStoreService'];

    public $onInit(): void {
        this.todos$ = this.todosStoreService.getTodos();
        this.todos$.subscribe((value: Todos) => {
            this.todos = value;
        });
    }

    public constructor(private todosStoreService: TodosService) { }

    public onAddNewTodoButtonClick(): void {
        this.todosStoreService.createNewTodo();
    }

    public onUpdate(todo: Todo): void {
        this.todosStoreService.updateTodo(todo);
    }

    public onDelete(todo: Todo): void {
        this.todosStoreService.deleteTodo(todo);
    }

    public onUpdateTodosNameButtonClick(newTodosName: string): void {
        this.todosStoreService.updateTodosName(newTodosName);
    }
}
