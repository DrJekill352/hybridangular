import { downgradeInjectable } from '@angular/upgrade/static';
import { module } from 'angular';

import { TodosComponent } from './todos.component';
import { TodosService } from '../../store';

export const TodosModule: string = 'todos-module';
module(TodosModule, [])
    .component('haTodos', new TodosComponent());
