import * as angular from 'angular';
import 'angular-mocks';

import { Todo } from '@todo-app';
import { todoList } from '../../../repository/todos-repository/todos-repository.mock';
import { NgModule } from '../../../main';
import { TodosRepositoryService } from '../../../repository';

describe('TodosComponent', () => {
    let component: any;

    beforeEach(angular.mock.module(NgModule));

    beforeEach(angular.mock.module(($provide: angular.IModule) => {
        let todosRepositoryService: TodosRepositoryService = new TodosRepositoryService();
        $provide.value('todosStoreService', todosRepositoryService);
    }));

    beforeEach(inject(($componentController: angular.IComponentControllerService) => {
        component = $componentController('haTodos', {}, {});
    }));

    it('should work', () => {
        expect(component).toBeDefined();
    });

    it('should on init', () => {
        expect(component.todos).toBeUndefined();
        component.$onInit();
        expect(component.todos).toEqual(todoList);
    });

    it('should on added todo button click', () => {
        let todosLength: number = todoList.todos.length;
        component.$onInit();
        expect(component.todos.todos.length).toEqual(todosLength);
        component.onAddNewTodoButtonClick();
        todosLength++;
        expect(component.todos.todos.length).toEqual(todosLength);
    });

    it('should on update click', () => {
        component.$onInit();
        let updatingTodo: Todo = todoList.todos[0];
        expect(component.todos.todos[0].isCompleted).toEqual(true);
        updatingTodo.isCompleted = false;
        component.onUpdate(updatingTodo);
        expect(component.todos.todos[0].isCompleted).toEqual(false);
    });

    it('should on delete click', () => {
        component.$onInit();
        let deletingTodo: Todo = todoList.todos[0];
        expect(component.todos.todos[0].id).toEqual(deletingTodo.id);
        component.onDelete(deletingTodo);
        expect(component.todos.todos[0].id).not.toEqual(deletingTodo.id);
    });

    it('should on update todos name click', () => {
        component.$onInit();
        let nameTodos: string = component.todos.name;
        nameTodos = 'New Todos Name';
        component.onUpdateTodosNameButtonClick(nameTodos);
        expect(component.todos.name).toEqual(nameTodos);
    });
});
