import { module } from 'angular';

import { ButtonComponent } from './button.component';

export const ButtonModule: string = 'button-module';
module(ButtonModule, [])
    .component('haButton', new ButtonComponent());
