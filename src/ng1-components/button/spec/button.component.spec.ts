import * as angular from 'angular';
import 'angular-mocks';

import { ButtonModule } from '../button.component.module';

describe('ButtonComponent', () => {
    let component: angular.IComponentController;
    beforeEach(angular.mock.module(ButtonModule));

    beforeEach(inject(($componentController: angular.IComponentControllerService) => {
        component = $componentController('haButton', {}, {});
    }));

    it('should work', () => {
        expect(component).toBeDefined();
    });
});
