import { IComponentOptions, IController, IControllerConstructor, Injectable } from 'angular';

import { Bindings } from '@bindings';

export class ButtonComponent implements IComponentOptions {
    public templateUrl: string = `src/ng1-components/button/button.component.html`;
    public controller: Injectable<IControllerConstructor> = ButtonComponentController;
    public bindings: Bindings = { content: '@' };
}
class ButtonComponentController implements IController {

}
