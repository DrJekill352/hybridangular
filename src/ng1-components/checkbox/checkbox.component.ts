import { IComponentOptions, IController, Injectable, IControllerConstructor } from 'angular';

import { Bindings } from '@bindings';

export class CheckboxComponent implements IComponentOptions {
    public templateUrl: string = `src/ng1-components/checkbox/checkbox.component.html`;
    public controller: Injectable<IControllerConstructor> = CheckboxComponentController;
    public bindings: Bindings = { isChecked: '<' };
}
class CheckboxComponentController implements IController {

}
