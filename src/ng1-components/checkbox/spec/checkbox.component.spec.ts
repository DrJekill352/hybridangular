import * as angular from 'angular';
import 'angular-mocks';

import { CheckboxModule } from '../checkbox.component.module';

describe('CheckboxComponent', () => {
    let component: angular.IComponentController;
    beforeEach(angular.mock.module(CheckboxModule));

    beforeEach(inject(($componentController: angular.IComponentControllerService) => {
        component = $componentController('haCheckbox', {}, {});
    }));

    it('should work', () => {
        expect(component).toBeDefined();
    });
});
