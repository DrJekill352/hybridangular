import { module } from 'angular';

import { CheckboxComponent } from './checkbox.component';

export const CheckboxModule: string = 'checkbox-module';
module(CheckboxModule, [])
    .component('haCheckbox', new CheckboxComponent());
